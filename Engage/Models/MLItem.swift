//
//  MLItem.swift
//  Expion
//
//  Created by Mike Bagwell on 12/16/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit

/**
 MLItem - Master List Item
 - parameter one: param one description
 - returns: Object that represents Master List Item
 */

@objcMembers class MLNavItem: Serializable { 
    let id: Int
    let display_name: String
    let icon_class_name: String
    let type: String
    let link_url: String
    let state_name: String
    let internal_attributes: Array<Dictionary<String, AnyObject>>
    let settings: String
    let order_ind: Int
    let shd_v: Bool
    let shd_e: Bool
    let shd_d: Bool
    let shd_c: Bool
    let set_hw_c: Bool
    let set_hw_v: Bool
    
//    let name: String
//    let suggested_ind: Bool
//    let code_class_name: String
// Workspace landing page
//    "folder_ind": false, //don't need
//    "id": 11532,
//    "company_id": 31, //don't need
//    "name": "1464",
//    "order_ind": -1,
//    "settings": "",
//    "state_name": "stacks",
//    "suggested_ind": false,
//    "shd_v": true,
//    "shd_e": true,
//    "shd_d": true,
//    "shd_c": true,
//    "set_hw_c": true,
//    "set_hw_v": true,
//    "code_class_name": "MLStack",
//    "deployed_already": false, //do we need?
//    "user_id": 2199, //do we need?
//    "actionList": { //do we need?
//    "actions": []
//    },
//    "internal_attributes": [{
//    "ml_attribute_name": "layoutId",
//    "ml_attribute_value": "11076"
//    }]
    
    
    
    init(_ dictionary: [String: Any]) {
        self.id = dictionary["id"] as? Int ?? 0
        self.display_name = dictionary["display_name"] as? String ?? ""
        self.icon_class_name = dictionary["icon_class_name"] as? String ?? ""
        self.type = dictionary["type"] as? String ?? ""
        self.link_url = dictionary["link_url"] as? String ?? ""
        self.state_name = dictionary["state_name"] as? String ?? ""
        self.internal_attributes = dictionary["internal_attributes"] as? Array<Dictionary<String, AnyObject>> ?? Array<Dictionary<String, AnyObject>>()
        self.settings = dictionary["settings"] as? String ?? ""
        self.order_ind = dictionary["order_ind"] as? Int ?? 0
        self.shd_v = dictionary["shd_v"] as? Bool ?? false
        self.shd_e = dictionary["shd_e"] as? Bool ?? false
        self.shd_d = dictionary["shd_d"] as? Bool ?? false
        self.shd_c = dictionary["shd_c"] as? Bool ?? false
        self.set_hw_c = dictionary["set_hw_c"] as? Bool ?? false
        self.set_hw_v = dictionary["set_hw_v"] as? Bool ?? false
        
//        self.name = dictionary["name"] as? String ?? ""
//        self.suggested_ind = dictionary["suggested_ind"] as? Bool ?? false
//        self.code_class_name = dictionary["code_class_name"] as? String ?? ""
    }
}

@objcMembers class MLItem: Serializable {  //MLStack
    /*
    let name: String
    let id: String
    let layout_id: String
    let image: UIImage?
    let selected: Bool
    let mlItemJSON: String
    let settings: MLSettings
    */
    
    var selected_ind: Bool
    let folder_ind: Bool
    let id: Int
    let company_id: Int
    let name: String
    let order_ind: Int
    let settings: String
    let suggested_ind: Bool
    let shd_v: Bool
    let shd_e: Bool
    let shd_d: Bool
    let shd_c: Bool
    let set_hw_c: Bool
    let set_hw_v: Bool
    let code_class_name: String
    let css_class_name: String
    let deployed_status: String
    
    let layout_id: Int
    
    // code_class_name = object representation of deserialized settings string by code_class_name value
    let mlStack: MLStack
    
    let children: Array<MLItem>
    
    // Workspace landing page
    //    "folder_ind": false, //don't need
    //    "id": 11532,
    //    "company_id": 31, //don't need
    //    "name": "1464",
    //    "order_ind": -1,
    //    "settings": "",
    //    "state_name": "stacks",
    //    "suggested_ind": false,
    //    "shd_v": true,
    //    "shd_e": true,
    //    "shd_d": true,
    //    "shd_c": true,
    //    "set_hw_c": true,
    //    "set_hw_v": true,
    //    "code_class_name": "MLStack",
    //    "deployed_already": false, //do we need?
    //    "user_id": 2199, //do we need?
    //    "actionList": { //do we need?
    //    "actions": []
    //    },
    //    "internal_attributes": [{
    //    "ml_attribute_name": "layoutId",
    //    "ml_attribute_value": "11076"
    //    }]
    
    
    init(folder_ind: Bool, selected_ind: Bool, id: Int, company_id: Int, name: String, order_ind: Int, settings: String, suggested_ind: Bool, shd_v: Bool, shd_e: Bool, shd_d: Bool, shd_c: Bool, set_hw_c: Bool, set_hw_v: Bool, code_class_name: String, css_class_name: String, deployed_status: String, layout_id: Int, children: Array<MLItem>,
       
        mlStack: MLStack){
        
        self.folder_ind = folder_ind
        self.selected_ind = selected_ind
        self.id = id
        self.company_id = company_id
        self.name = name
        self.order_ind = order_ind
        self.settings = settings
        self.suggested_ind = suggested_ind
        self.shd_v = shd_v
        self.shd_e = shd_e
        self.shd_d = shd_d
        self.shd_c = shd_c
        self.set_hw_c = set_hw_c
        self.set_hw_v = set_hw_v
        self.code_class_name = code_class_name
        self.css_class_name = css_class_name
        
        self.deployed_status = deployed_status
        self.layout_id = layout_id
        
        self.children = children
        //this is defined by code_class_name
        self.mlStack = mlStack
    }
    
    init(_ dictionary: [String: Any]) {
        //this selected_ind might need to change
        self.selected_ind = dictionary["selected_ind"] as? Bool ?? false
        self.folder_ind = dictionary["folder_ind"] as? Bool ?? false
        self.id = dictionary["id"] as? Int ?? 0
        self.company_id = dictionary["company_id"] as? Int ?? 0
        self.name = dictionary["name"] as? String ?? ""
        self.order_ind = dictionary["order_ind"] as? Int ?? 0
        self.settings = dictionary["settings"] as? String ?? ""
        self.suggested_ind = dictionary["suggested_ind"] as? Bool ?? false
        self.shd_v = dictionary["shd_v"] as? Bool ?? false
        self.shd_e = dictionary["shd_e"] as? Bool ?? false
        self.shd_d = dictionary["shd_d"] as? Bool ?? false
        self.shd_c = dictionary["shd_c"] as? Bool ?? false
        self.set_hw_c = dictionary["set_hw_c"] as? Bool ?? false
        self.set_hw_v = dictionary["set_hw_v"] as? Bool ?? false
        self.code_class_name = dictionary["code_class_name"] as? String ?? ""
        self.css_class_name = dictionary["css_class_name"] as? String ?? ""
        
        self.deployed_status = dictionary["deployed_status"] as? String ?? ""
        if (dictionary["layout_id"] != nil){
            self.layout_id = dictionary["layout_id"] as? Int ?? 0
        } else {
            if let internal_attributes = dictionary["internal_attributes"] as? Array<Dictionary<String, Any>> {
                print("")
                print(internal_attributes)
                if(internal_attributes.count > 0) {
                    if let dict = internal_attributes[0] as? Dictionary<String, Any> {
                        if let ml_attribute_name = dict["internal_attributes"] as? String {
                            if ml_attribute_name.lowercased().contains("layoutId"){
                                if let jsonLayoutId = dict["ml_attribute_value"] as? String {
                                    self.layout_id = Int(jsonLayoutId)!
                                } else { self.layout_id = 0 }
                            } else { self.layout_id = 0 }
                        } else { self.layout_id = 0 }
                    } else { self.layout_id = 0 }
                } else { self.layout_id = 0 }
            } else { self.layout_id = 0 }
        }
        
        self.children = dictionary["children"] as? Array<MLItem> ?? []
        //this is defined by code_class_name
        let attributes = MLAttributes(layout_id: self.layout_id)
        let directive = MLDirective(directive_name: "", attributes: attributes)
        self.mlStack = dictionary["mlStack"] as? MLStack ?? MLStack(directive: directive)
    }
}

@objcMembers class MLStack: Serializable {
    //MLStack
    //"code_class_name": "home"
    //{\"directive\":{\"directive_name\":\"expion-iframe\",\"attributes\":{\"webUrl\":\"/Home/Home?company_id=%%COMPANY_ID%%\"},\"disable_cache\":true}}",
    //"{\"directive\":{\"directive_name\":\"stacks-layout\",\"attributes\":{\"layout_id\":4746}}}"
    
    let directive: MLDirective
    
    init(directive: MLDirective) {
        self.directive = directive
    }
}

@objcMembers class MLDirective: Serializable {
    
    let directive_name: String
    let attributes: MLAttributes
    let webUrl: String
//    let attributesArray: NSMutableArray
    
    //TODO: create attributesArray: NSMutableArray on both sides
    init(directive_name: String, attributes: MLAttributes) { //, attributesArray: NSMutableArray) {
        self.directive_name = directive_name
        self.attributes = attributes
        self.webUrl = ""
        //        self.attributesArray = attributesArray
    }
    
    init(directive_name: String, attributes: MLAttributes, webUrl: String) { //, attributesArray: NSMutableArray) {
        self.directive_name = directive_name
        self.attributes = attributes
        self.webUrl = webUrl
        //        self.attributesArray = attributesArray
    }
}

@objcMembers open class MLAttributes: Serializable {
    let layout_id: Int
    //let data_single_stack_id: Int
    //let module_id: Int
    let attributeValues: Array<MLAttributesKeyValue>!
    
    init(layout_id: Int){//, attributValues: Array<MLAttributesKeyValue>!) {
        //init(layout_id: Int, data_single_stack_id: Int, module_id: Int) {
        self.layout_id = layout_id
        //self.data_single_stack_id = data_single_stack_id
        //self.module_id = module_id
        self.attributeValues = nil
    }
    
    init(layout_id: Int, attributeValues: Array<MLAttributesKeyValue>!) {
        self.layout_id = layout_id
        self.attributeValues = attributeValues
    }
    
    /*
    let webUrl: String
    let disable_cache: Bool
    
    init(webUrl: String, disable_cache: Bool) {
    self.webUrl = webUrl
    self.disable_cache = disable_cache
    }*/
}

@objcMembers class MLAttributesKeyValue: Serializable {
    let key: String
    let value: String
    
    init(key: String, value: String){
        self.key = key
        self.value = value
    }
    
}

@objcMembers class SerializableMLItem: Serializable
{
    var item: MLItem!
    
    override init()
    {
        
    }
}








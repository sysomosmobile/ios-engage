//
//  LoginViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 12/15/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit
import WebKit

@objc
protocol LoginViewControllerDelegate {
    func loginComplete()
}

class LoginViewController: UIViewController, UITextFieldDelegate, AuthenticationLoginDelegate {
    var delegate: LoginViewControllerDelegate?
    var urlAPI: String!
    var urlFE: String!
    var actionUrl: String! //TODO: remove me?
    var company_id: String!
    var expionName: String!

    var accessToken: String!
    
    let serialQueue = DispatchQueue(label:"loginQueue")

    var autoLogin: Bool = false
    var savePassword: Bool = true
    
    var overrideTimer: Timer!
    var overrideCount: Int = 0
    
    //----urlsession
    let opQueue = OperationQueue()
    var response:URLResponse?
    var session:URLSession?
    
    @IBOutlet weak var loginLogo: UIButton!
    @IBOutlet weak var scrollthis: UIScrollView!
    @IBOutlet weak var loginLoadingView: UIView!
    @IBOutlet weak var loginPage: UIView!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var serverCodeField: UITextField!
    @IBOutlet weak var branchField: UITextField!
    @IBOutlet weak var savePasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var debugMode: UISwitch!
    
    @IBOutlet weak var topPosCons: NSLayoutConstraint!
    
    @IBAction func savePasswordBtn(_ sender: AnyObject) {
        savePasswordToggle()
    }
    
    func savePasswordToggle() {
        savePassword = !savePassword
        if(savePassword == false) {
            self.clearSavedPassword()
        }
        UserDefaults.standard.setValue(savePassword, forKey: "savePassword")
        setPasswordTogggle()
    }
    
    func setPasswordTogggle() {
        DispatchQueue.main.async {
            if(self.savePassword == true) {
                self.savePasswordButton.setImage(UIImage(named: "checkBoxChecked.png")!, for: .normal)
            } else {
                self.savePasswordButton.setImage(UIImage(named: "checkBoxUnchecked.png")!, for: .normal)
            }
        }
    }
    
    @IBAction func loginBtn(_ sender: AnyObject) {
        checkLogin()
    }
    
    @IBAction func loginLogoButton(_ sender: AnyObject) {
        setOverrideTimer()
    }
    
    func setOverrideTimer() {
        if(overrideTimer == nil) {
            //print("set overrideTimer")
            overrideTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(checkOverrideCount), userInfo: nil, repeats: false)
        } else {
            //print("overrideTimer != nil")
        }
        overrideCount += 1
    }
    
    //@objc
    @objc func checkOverrideCount() {
        overrideTimer.invalidate()
        overrideTimer = nil
        //print("overrideTimer finished count=>" + String(overrideCount))
        if(overrideCount > 2) {
            showBranch()
        } else {
            hideBranch()
        }
        overrideCount = 0
    }
    
    @IBAction func logoutBtn(_ sender: AnyObject) {
        logout()
    }
    
    func logout() {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFromKeyChain()
        setLoginLogoImg()
        // Do any additional setup after loading the view, typically from a nib.
        setColorScheme()
    }
    
    func setColorScheme(){
        DispatchQueue.main.async {
            self.loginPage.backgroundColor = ShareData.sharedInstance.primaryColor
        }
    }
    
    func setLoginLogoImg() {
        DispatchQueue.main.async {
            self.loginLogo.imageView?.contentMode = .scaleAspectFit
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupView()
        setLoginLogoImg()
    }
    
    func setupView() {
        DispatchQueue.main.async {
            self.usernameField.delegate = self
            self.passwordField.delegate = self
            self.serverCodeField.delegate = self
            self.branchField.delegate = self
        }
        disableScroll()
    }
    
    func enableScroll(){
        DispatchQueue.main.async {
            self.scrollthis.contentSize = CGSize(width: self.scrollthis.frame.width, height: 540)
        }
    }
    
    func disableScroll(){
        DispatchQueue.main.async {
            self.scrollthis.contentSize = CGSize(width: self.scrollthis.frame.width, height: self.scrollthis.frame.height)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DispatchQueue.main.async {
            textField.resignFirstResponder()
        }
        checkLogin()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            self.topPosCons.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
                self.scrollthis.frame.origin.y = 0
                self.scrollthis.layoutIfNeeded()
            })
        }
        enableScroll()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            self.topPosCons.constant = 106
            UIView.animate(withDuration: 0.2, animations: {
                self.scrollthis.frame.origin.y = 106
                self.scrollthis.layoutIfNeeded()
            })
        }
        disableScroll()
    }
    
    func serverCodeFieldDidChange(_ textField: UITextField) {
        DispatchQueue.main.async {
            self.branchField.text = ""
            self.checkForBranch()
        }
    }
    
    func checkForBranch() {
        let code: String = (branchField.text?.replacingOccurrences(of: " ", with: ""))!
        if(code.count > 0) {
            showBranch()
        } else {
            if(self.branchField.text?.count == 0) {
                hideBranch()
            }
        }
    }
    
    func showBranch() {
        DispatchQueue.main.async {
            self.branchField.isHidden = false
            self.serverCodeField.isHidden = false
            self.debugMode.isHidden = false
            self.debugMode.isEnabled = true
        }
    }
    
    func hideBranch() {
        DispatchQueue.main.async {
            self.branchField.isHidden = true
            self.branchField.text = "";
            self.serverCodeField.isHidden = true
            self.serverCodeField.text = ""
            self.debugMode.isHidden = true
            self.debugMode.isEnabled = false
            self.debugMode.setOn(false, animated: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkLogin() {
        DispatchQueue.main.async {
            self.loginLoadingView.isHidden = false
            self.usernameField.resignFirstResponder()
            self.passwordField.resignFirstResponder()
            self.branchField.resignFirstResponder()
            self.serverCodeField.resignFirstResponder()
        }
        var enteredBranch: String!
        enteredBranch = branchField.text
        
        loginSysomos(expionUrl: "", sysOAuthDomain: enteredBranch)
    }
    
    func loginSysomos(expionUrl: String, sysOAuthDomain: String) {
        var enteredUsername: String!
        var enteredPassword: String!
        var oauthCode: String!
        var cliqueOverride: String!

        if((usernameField.text?.count)! > 0 && (passwordField.text?.count)! > 0){
            
            enteredUsername = usernameField.text
            enteredPassword = passwordField.text
            oauthCode = branchField.text
            cliqueOverride = serverCodeField.text
            
            /*
             * Login for sysomos sso test
             *
             *//*
            //let productionTest: Bool = false
            //var sysDomain: String!
            if(productionTest == true) {
                sysDomain = "oauth.sysomos.com"
                enteredUsername = "mguglietti@sysomos.com"
                enteredPassword = "o,IiFo84"
            } else {
                sysDomain = sysOAuthDomain
            }
             */
            
            let authLogin: AuthenticationLogin = AuthenticationLogin()
            authLogin.delegate = self
            authLogin.ssoBegin(eUsername: enteredUsername, ePW: enteredPassword, oauthCode: oauthCode, cliqueOverride: cliqueOverride)
        } else {
            showAlert(alertTitle: "Missing Info", alertMessage:"Username and password are required to login.  Please check your entry and try again.")
        }
    }
    
    func authenticationComplete(aToken: String, companyId: Int, SSOBaseUrl: String, FEBaseUrl: String) {
        serialQueue.sync {
            self.accessToken = aToken
            self.company_id = String(companyId)
            
            let index = SSOBaseUrl.index(SSOBaseUrl.startIndex, offsetBy: 2)
            let subStr = String(SSOBaseUrl[..<index])
            if (subStr == "//") {
                DispatchQueue.main.async {
                    let sCode: String = self.serverCodeField.text!
                    if(sCode.count > 0 && sCode.lowercased() == "local") {
                        self.urlAPI = String(format:"http:%@", SSOBaseUrl)
                        self.urlFE = String(format:"http:%@", FEBaseUrl)
                    } else {
                        self.urlAPI = String(format:"https:%@", SSOBaseUrl)
                        self.urlFE = String(format:"http:%@", FEBaseUrl)
                    }
                }
            } else {
                self.urlAPI = SSOBaseUrl
                self.urlFE = FEBaseUrl
            }
            self.storeToKeyChain()
        }
        serialQueue.sync {
            self.loginComplete()
        }
    }
    
    func authenticationFailed(errorMessage: String) {
        showAlert(alertTitle: "Login Failed", alertMessage:errorMessage as String)
    }
    
    func mobileVersionInvalid() {
        let expionVersion = ExpionVersion()
        expionVersion.vc = self
        expionVersion.showForceUpdateAlert()
    }
    
    func jankAccessTokenComplete(_ aToken: String, companyId: String) {
        accessToken = aToken
        company_id = companyId
        serialQueue.sync {
            //print("jankAccessTokenComplete-storeToKeyChain start")
            self.storeToKeyChain()
            //print("jankAccessTokenComplete-storeToKeyChain complete")
        }
        serialQueue.sync {
            //print("jankAccessTokenComplete-loginComplete start")
            self.loginComplete()
            //print("jankAccessTokenComplete-loginComplete end")
        }
    }
    
    func storeToKeyChain() {
//        let keychain: KeychainItemWrapper = KeychainItemWrapper.init(identifier: "MWEngage", accessGroup: nil)
        
        var username: String!
//        var pwTemp: String!
        var password: String!
        var serverCode: String!
        var branch: String!
        var dMode: NSNumber = 0
        
        DispatchQueue.main.async {
            username = (self.usernameField.text?.count)! > 0 ? self.usernameField.text! : ""
            password = (self.passwordField.text?.count)! > 0 ? self.passwordField.text! : ""
            serverCode = (self.serverCodeField.text?.count)! > 0 ? self.serverCodeField.text! : ""
            branch = (self.branchField.text?.count)! > 0 ? self.branchField.text! : ""
        
        
            if(self.savePassword == false) {
                password = ""
            }
            
            //print("storeToKeyChain")
            //print(serverCode)
            //print(branch)
            
            let keychain = KeychainWrapper.standard
            if let pwData = password.data(using: .utf8) {
                keychain.set(pwData, forKey: "key")
            } else {
                keychain.set("", forKey: "key")
            }
            keychain.set(username, forKey: "account")
            keychain.set(serverCode, forKey: "serverCode")
//            keychain.set("ExpionMobile", forKey: "service")
            keychain.set(branch, forKey: "branch")
            
//            keychain.setObject((username as NSString), forKey: kSecAttrAccount)
//            keychain.setObject(password, forKey: kSecValueData)
//            keychain.setObject(serverCode, forKey: kSecAttrDescription)
//            keychain.setObject("ExpionMobile", forKey: kSecAttrService)
//            keychain.setObject(branch, forKey: kSecAttrComment)

            dMode = self.debugMode.isOn == true ? 1 : 0
        
            //print("dMode=>\(dMode)")
        
            UserDefaults.standard.setValue(self.urlAPI, forKey: "urlAPI")
            UserDefaults.standard.setValue(self.urlFE, forKey: "urlFE")
            UserDefaults.standard.setValue(self.company_id, forKey: "companyId")
            UserDefaults.standard.setValue(username, forKey: "userName")
            UserDefaults.standard.setValue(dMode, forKey: "debugMode")
        }
    }
    
    func clearSavedPassword() {
//        let keychain: KeychainItemWrapper = KeychainItemWrapper.init(identifier: "MWEngage", accessGroup: nil)
//        keychain.setObject("", forKey: kSecValueData)
        let keychain = KeychainWrapper(serviceName: "MWEngage")
        keychain.set("", forKey: "key")
    }
    
    func loginComplete(){
        DispatchQueue.main.async {
            self.usernameField.delegate = nil
            self.passwordField.delegate = nil
            self.serverCodeField.delegate = nil
            self.branchField.delegate = nil
        
            self.dismiss(animated: true) {
                //self.delegate!.dismissViewController()
                self.loginLoadingView.isHidden = true
                self.delegate?.loginComplete()
                self.delegate = nil
            }
        }
    }
    
    func getFromKeyChain() {
//        let keychain: KeychainItemWrapper = KeychainItemWrapper.init(identifier: "ExpionMobile", accessGroup: nil)
        
        let keychain = KeychainWrapper.standard
        
        DispatchQueue.main.async {
//            if let username = keychain.object(forKey: kSecAttrAccount) as? String {
//                self.usernameField.text = username
//            }
//            if let passwordData = keychain.object(forKey: kSecValueData) as? NSData {
//                if let password = String(data: passwordData as Data, encoding: .utf8) {
//                    self.passwordField.text = password
//                }
//            }
//            if let branch = keychain.object(forKey: kSecAttrComment) as? String {
//                self.branchField.text = branch
//                if let serverCode = keychain.object(forKey: kSecAttrDescription) as? String {
//                    if(serverCode.count > 0) {
//                        self.serverCodeField.text = serverCode
//                    } else {
//                        self.serverCodeField.text = ""
//                    }
//                }
//            } else {
//                self.serverCodeField.text = ""
//            }
            
            if let username = keychain.string(forKey: "account") {
                self.usernameField.text = username
            }
            if let passwordData = keychain.data(forKey: "key") {
                if let password = String(data: passwordData as Data, encoding: .utf8) {
                    self.passwordField.text = password
                }
            }
            if let branch = keychain.string(forKey: "branch") {
                self.branchField.text = branch
                if let serverCode = keychain.string(forKey: "serverCode") {
                    if(serverCode.count > 0) {
                        self.serverCodeField.text = serverCode
                    } else {
                        self.serverCodeField.text = ""
                    }
                }
            } else {
                self.serverCodeField.text = ""
            }
            
            self.checkForBranch()
            
            if let savedPW = UserDefaults.standard.object(forKey: "savePassword"){
                self.savePassword = savedPW as! Bool
            }
            self.setPasswordTogggle()
        }
    }
    
    func showAlert(alertTitle: String, alertMessage: String) {
        //print("!!--showAlert =>\(alertTitle)-- \(alertMessage)")
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
            
            // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
                //print("OK")
                self.alertOkAction()
            }
            alertController.addAction(okAction)
//            self.present(alertController, animated: true, completion: nil)
            
            if self.presentedViewController == nil {
                self.present(alertController, animated: true, completion: nil)
            } else{
                self.dismiss(animated: false) { () -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func alertOkAction() {
        DispatchQueue.main.async {
            self.loginLoadingView.isHidden = true
            self.loginButton.isEnabled = true
        }
    }
}

extension String {
    
    func textFromHexString() -> String? {
        guard let chars = cString(using: String.Encoding.utf8) else { return nil}
        var i = 0
        let length = count
        
        let data = NSMutableData(capacity: length/2)
        var byteChars: [CChar] = [0, 0, 0]
        
        var wholeByte: CUnsignedLong = 0
        
        while i < length {
            byteChars[0] = chars[i]
            i+=1
            byteChars[1] = chars[i]
            i+=1
            wholeByte = strtoul(byteChars, nil, 16)
            data?.append(&wholeByte, length: 1)
        }
        
        return String(data: data! as Data, encoding: String.Encoding.utf8) as String?
    }
}
/*
extension LoginViewController:URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        // We've got an error
        if error != nil {
            //print("Error: \(err.localizedDescription)")
        } else {
            //print("Error. Giving up")
        }
        //            PlaygroundPage.current.finishExecution()
    }
}

extension LoginViewController:URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        // The task became a stream task - start the task
        //print("didBecome streamTask")
        streamTask.resume()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        // The task became a download task - start the task
        //print("didBecome downloadTask")
        downloadTask.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        // The original request was redirected to somewhere else.
        // We create a new dataTask with the given redirection request and we start it.
        if let urlString = request.url?.absoluteString {
            //print("willPerformHTTPRedirection to \(urlString)")
        } else {
            //print("willPerformHTTPRedirection")
        }
        if let task = self.session?.dataTask(with: request) {
            task.resume()
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // We've got an error
        if let err = error {
            //print("Error: \(err.localizedDescription)")
        } else {
            //print("Error. Giving up")
        }
        //            PlaygroundPage.current.finishExecution()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        // We've got the response headers from the server.
        //print("didReceive response")
        self.response = response
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        // We've got the response body
        //print("didReceive data")
        if let responseText = String(data: data, encoding: .utf8) {
            //print(self.response ?? "")
            //print("\nServer's response text")
            //print(responseText)
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            //                if let firstCategory = json["categories"]??[0]["name"] as? String {
            //                    //print("The first category in the JSON list is \"\(firstCategory)\"")
            //                }
            //print("this is the json")
            //print(json)
        } catch let error as NSError {
            //print("Error parsing JSON: \(error.localizedDescription)")
        }
        
        self.session?.finishTasksAndInvalidate()
        //            PlaygroundPage.current.finishExecution()
    }
    
    
}
    
    
extension LoginViewController:WKNavigationDelegate {
    
    //----------------------------
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //print("!!--didStartProvisionalNavigation--!!")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        //print("!!--didFail navigation--!!")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        //print("!!--decidePolicyFor navigationResponse--!!")
        if let urlResponse = navigationResponse.response as? HTTPURLResponse,
            let url = urlResponse.url,
            let allHeaderFields = urlResponse.allHeaderFields as? [String : String] {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: allHeaderFields, for: url)
            
            HTTPCookieStorage.shared.setCookies(cookies , for: urlResponse.url!, mainDocumentURL: nil)
            //print("decisionHandler-cookies=>")
            //print(allHeaderFields)
            decisionHandler(.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        //print("!!--didReceive challenge--!!")
        let cred = URLCredential.init(trust: challenge.protectionSpace.serverTrust!)
        completionHandler(.useCredential, cred)
    }
    
    
    //    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    //        //print("decidePolicyFor navigationAction")
    //        if let urlRequest = navigationAction.request as? URLRequest,
    //            let url = navigationAction.request.url,
    //            let allHeaderFields = urlRequest.allHTTPHeaderFields as? [String : String] {
    //            let cookies = HTTPCookie.cookies(withResponseHeaderFields: allHeaderFields, for: url)
    //
    //            HTTPCookieStorage.shared.setCookies(cookies , for: url, mainDocumentURL: nil)
    //            //print("decisionHandler-cookies=>")
    //            //print(allHeaderFields)
    //            decisionHandler(.allow)
    //        }
    //    }
    
    func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        //print("webView:\(webView) decidePolicyForNavigationAction:\(navigationAction) decisionHandler:\(decisionHandler)")
        
        //        switch navigationAction.navigationType {
        //        case .LinkActivated:
        //            if navigationAction.targetFrame == nil {
        //                self.webView?.loadRequest(navigationAction.request)
        //            }
        //            if let url = navigationAction.request.URL where !url.absoluteString.hasPrefix("http://www.myWebSite.com/exemlpe") {
        //                UIApplication.sharedApplication().openURL(url)
        //                //print(url.absoluteString)
        //                decisionHandler(.Cancel)
        //                return
        //            }
        //        default:
        //            break
        //        }
        
        
        
        if let url = navigationAction.request.url {
            //print(url.absoluteString)
            
        }
        
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //print("Finished navigating to url \(String(describing: webView.url))")
//        self.hideLoading()
//        showContentView()
        
        //        if(cookieInjected == false){
        //            injectCookie()
        //        } else {
        //            injectScriptWK()
        //        }
        
        //        injectScriptWK()
    }
    
    
    
}
 */

//
//  AuthenticationLogin.swift
//  Expion
//
//  Created by Mike Bagwell on 7/27/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation


@objc
protocol AuthenticationLoginDelegate {
    @objc optional func authenticationComplete(aToken: String, companyId: Int, SSOBaseUrl: String, FEBaseUrl: String) //unused
    @objc optional func authenticationFailed(errorMessage: String)
    @objc optional func mobileVersionInvalid()
}

class AuthenticationLogin: NSObject {

    let opQueue = OperationQueue()
    var response:URLResponse?
    var session:URLSession?
    
    private let authQueue = DispatchQueue(label: "async.operation.queue")
    
    var delegate: AuthenticationLoginDelegate?
    
    
    var access_token: String = ""
    var isTestingUrl: Bool = false
    var authenticationFailed: Bool = false
    
    var login_session:String = ""
    var m_oauthCode: String = ""
    var cliqueOverR: String = ""
    
    var time:DispatchTime! {
        return DispatchTime.now() + 1.0 // seconds
    }
    
    func ssoBegin(eUsername: String, ePW: String, oauthCode: String, cliqueOverride: String) {
        let sessionConfiguration = URLSessionConfiguration.default
        // disable the caching
        sessionConfiguration.urlCache = nil
        sessionConfiguration.timeoutIntervalForRequest = 300
        sessionConfiguration.timeoutIntervalForResource = 600
        // initialize the URLSession
        self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: self.opQueue)
        
        postLoginDo(eUsername: eUsername, ePW: ePW, oauthCode: oauthCode, cliqueOverride: cliqueOverride)
//        start2FactorJankGet(eUsername: eUsername, ePW: ePW, oauthCode: oauthCode, cliqueOverride: cliqueOverride)
    }
    
    func doJankGet() {
        
    }
    
    func start2FactorJankGet(eUsername: String, ePW: String, oauthCode: String, cliqueOverride: String) {
        var urlPath: String = ""
        var baseOauthUrl: String = ""
        if(oauthCode.count > 0) {
            self.m_oauthCode = oauthCode

            baseOauthUrl = String(format:"https://qa-oauth.sysomos.com/oauth/login.jsp?platform=true", oauthCode)
            
            if(oauthCode.lowercased().contains("qa")){
                //TODO: check if necessary
                isTestingUrl = true
            }
        } else {
            baseOauthUrl = "https://oauth.sysomos.com"
        }
        if(cliqueOverride.count > 0) {
            self.cliqueOverR = cliqueOverride
        }
        
//        urlPath = String(format:"%@/oauth/login.do", baseOauthUrl)
        urlPath = baseOauthUrl
        if let url = URL(string: urlPath) {
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
            urlRequest.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")

            let task = self.session?.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in

                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode

                print("httpResponse=>")
                print(httpResponse)


                if (statusCode == 200) {
                    self.postLoginDo(eUsername: eUsername, ePW: ePW, oauthCode: oauthCode, cliqueOverride: cliqueOverride)
                } else if (statusCode == 401) {

                    // Save the incoming HTTP Response

//                    var httpResponse: NSHTTPURLResponse = response as! NSHTTPURLResponse

                    // Since the incoming cookies will be stored in one of the header fields in the HTTP Response, parse through the header fields to find the cookie field and save the data

                    let cookies = HTTPCookie.cookies(withResponseHeaderFields: httpResponse.allHeaderFields as! [String : String], for: (response?.url!)!)

                    HTTPCookieStorage.shared.setCookies(cookies , for: response?.url!, mainDocumentURL: nil)

                    self.postLoginDo(eUsername: eUsername, ePW: ePW, oauthCode: oauthCode, cliqueOverride: cliqueOverride)
                } else {
                    //print("Load error")
                }
                
            }
            
            task?.resume()
        }
    }
    
    func postLoginDo(eUsername: String, ePW: String, oauthCode: String, cliqueOverride: String) {
        self.opQueue.isSuspended = true
        
        
        // create a URLRequest with the given URL and initialize a URLSessionDataTask
        
//        let urlPath = sOauthDomain
        
//        let sharedSession = URLSession.shared
//
//
//        let cookies = HTTPCookieStorage.shared.cookies!
//        for cookie in cookies {
//            let key: String = "ASPXAUTH"
//                        //print("---oauth c---")
//                        //print(cookie.name)
//                        //print(cookie)
////            if(cookie.name.contains(key)){
////                let prefDefault = UserDefaults.standard
////                prefDefault.set(cookie.value, forKey: key as String)
////            }
//        }
        
        
        var urlPath: String = ""
        var baseOauthUrl: String = ""
        if(oauthCode.count > 0) {
            self.m_oauthCode = oauthCode
            baseOauthUrl = String(format:"https://%@-oauth.sysomos.com", oauthCode)
            if(oauthCode.lowercased().contains("qa")){
                //TODO: check if necessary
                isTestingUrl = true
            }
        } else {
            baseOauthUrl = "https://oauth.sysomos.com"
        }
        if(cliqueOverride.count > 0) {
            self.cliqueOverR = cliqueOverride
        }
        
        urlPath = String(format:"%@/oauth/login.do", baseOauthUrl)
//        urlPath = String(format:"%@/oauth/login.jsp", baseOauthUrl)
        
//        http://integration-oauth.sysomos.com/oauth/oauth/authorize?response_type=token&state=bd0a6b89-b511-4bf5-aba3-5a02d3829317&scope=read&client_id=sysomos-ui&redirect_uri=https%3A%2F%2Fintegration-app.sysomos.com%2F
        
        print("urlPath=>" + urlPath)
        if let url = URL(string: urlPath) {

            let request = NSMutableURLRequest(url: url)
            
            request.httpMethod = "POST"
            
            request.addValue("application/json, text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8", forHTTPHeaderField: "Accept")
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
//            request.addValue("1", forHTTPHeaderField: "DNT")
//            request.addValue("oauth.sysomos.com", forHTTPHeaderField: "Host")
//            request.addValue("https://oauth.sysomos.com", forHTTPHeaderField: "Origin")
            request.addValue(baseOauthUrl, forHTTPHeaderField: "Origin")
            request.addValue("no-cache", forHTTPHeaderField: "Pragma")
            request.addValue(String(format:"%@/oauth/login.jsp?platform=true", baseOauthUrl), forHTTPHeaderField: "Referer")
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.addValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
            
            let post_data: NSDictionary = NSMutableDictionary()
            
//            let userName = "jfain@sysomos.com"
//            let password = "Sysomos6!"
            
            post_data.setValue(eUsername, forKey: "j_username")
            post_data.setValue(ePW, forKey: "j_password")
            post_data.setValue("Submit", forKey: "login")
            
            var paramString = ""
            
            
            for (key, value) in post_data
            {
                paramString = paramString + "&" + (key as! String) + "=" + (value as! String)
            }
            
            //print("paramString=>" + paramString)
            
            request.httpBody = paramString.data(using: String.Encoding.utf8)

            
            if let task = self.session?.dataTask(with: request as URLRequest) {
//            if let task = sharedSession.dataTask(with: request as URLRequest) {
                task.resume()
            }
        }
        
        // Open the operations queue after 1 second
        DispatchQueue.main.asyncAfter(deadline: self.time, execute: {[weak self] in
            //print("Opening the OperationQueue")
            self?.opQueue.isSuspended = false
        })
    }
    
    func checkResponseUrl(responseUrl: String, responseTxt: String) -> Bool {
        print("<<--checkResponseUrl=>" + responseUrl)
        
        if(responseUrl.contains("menu.sysomos.com/content/SysWebSiteLogCallBack.html#access_token=")) {
            //access_token received
            authQueue.async {
                self.getAccessTokenFromUrlStr(str: responseUrl)
                self.getCompanyInfoFromMenu()
            }
            return true
        } else if (responseUrl.contains("/oauth/login.jsp?authorization_error=true") || responseUrl.contains("/oauth/login.jsp?authentication_error=true")) {
            var errorMsg: String = ""
            if (responseUrl.contains("&locked=true")){
                errorMsg = "This account has been locked because the maximum number of login attempts has been exceeded."
            } else {
                errorMsg = "Invalid username and password combination."
            }
            
            authenticationFailed(errorMessage: errorMsg)
            return false
        } else if (responseUrl.contains("/oauth/login.jsp")) {
            //print("hit login.jsp")
            let errorMsg: String = String(format: "Failed to complete request response from %@", (responseUrl))
            authenticationFailed(errorMessage: errorMsg)
            return false
        }
        return true
    }
    
    func authenticationFailed(errorMessage: String) {
        if(authenticationFailed == false) {
            authenticationFailed = true
            self.delegate?.authenticationFailed!(errorMessage: errorMessage)
        }
    }
    
    func getAccessTokenFromUrlStr(str: String) {
        //print("getAccessTokenFromUrlStr")
        let responseArr = str.components(separatedBy: "#access_token=")
        let atArr = responseArr[1].components(separatedBy: "&")
        let accessToken = atArr[0]
        if (accessToken.count > 0) {
            self.access_token = accessToken
            UserDefaults.standard.setValue(self.access_token, forKey: "accessToken")
        }
        //print("accessToken=>" + accessToken)
    }
    
    func getCompanyInfoFromMenu() {
        //print("getCliqueFromMenu=>" + access_token)
        var returnJSON: NSDictionary!
        
        self.opQueue.isSuspended = true
        let sessionConfiguration = URLSessionConfiguration.default
        // disable the caching
        sessionConfiguration.urlCache = nil
        
        // initialize the URLSession
        self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: self.opQueue)
        
//        let urlPath = "https://menu.sysomos.com/api/ml/apiurl?token=" + self.access_token
        
        var urlPath: String = ""
        if(m_oauthCode.count > 0) {
//            urlPath = String(format:"https://%@-menu.sysomos.com/api/ml/apiurl?token=%@", self.m_oauthCode, self.access_token)
            urlPath = String(format:"https://%@-menu.sysomos.com/api/ml/apiurl?token=%@", self.m_oauthCode, self.access_token)
            //            urlPath = String(format:"https://%@/oauth/login.do", oauthCode)
        } else {
            urlPath = "https://menu.sysomos.com/api/ml/apiurl?token=" + self.access_token
        }
        
        
//        //print("urlPath=>" + urlPath)
//        //print("")
        if let url = URL(string: urlPath) {
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
            urlRequest.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
//            //print(ShareData.sharedInstance.userAgent)
            let session = URLSession.shared
            let task = session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in
                
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                

                //print(httpResponse)
                //print("try parse=>")

                
                if (statusCode == 200) {
//                    //print("--success-->")
                    
                    
//                    let input = "{\"ResponseStatus\":1,\"ResponseData\":{\"company_id\":31,\"SSOBaseUrl\":\"//t1-social.sysomos.com\",\"MobileIsValid\":\"false\"}}"
//                    let data = input.data(using: .utf8)!
                    
                    
                    do{
                        returnJSON = try JSONSerialization.jsonObject(with: data! as Data, options:[]) as? NSDictionary
//                        returnJSON = try JSONSerialization.jsonObject(with: data as Data, options:[]) as? NSDictionary
                    } catch {
                        //print("Error with Json: \(error)")
                    }
                    
                    if returnJSON != nil {
                        //print(returnJSON)
                        if let responseData = returnJSON["ResponseData"] as? NSDictionary {
//                            //print("responseData found")
//                            //print(responseData)

                            var company_id: Int = 0
                            var SSOBaseUrl: String = ""
                            if let mobileIsValid = responseData["MobileIsValid"] as? String {
                                if(mobileIsValid.lowercased() == "false") {
                                    self.delegate?.mobileVersionInvalid!()
                                    return
                                }
                            }
                            if let companyId_Temp = responseData["company_id"] as? Int {
                                company_id = companyId_Temp
                            }
                            if let SSOBaseUrl_Temp = responseData["SSOBaseUrl"] as? String {
                                if(self.cliqueOverR.count > 0) {
                                    SSOBaseUrl = String(format: "//%@-social.sysomos.com", self.cliqueOverR)
                                    //                                    SSOBaseUrl = SSOBaseUrl_Temp
                                } else {
                                    SSOBaseUrl = SSOBaseUrl_Temp
                                }
                            }
                            
                            if let httpResponse = response as? HTTPURLResponse, let fields = httpResponse.allHeaderFields as? [String : String] {
                                let cookies = HTTPCookie.cookies(withResponseHeaderFields: fields, for: response!.url!)
                                
                                var cSSOBaseUrl: String = ""
                                let index = SSOBaseUrl.index(SSOBaseUrl.startIndex, offsetBy: 2)
                                let subStr = String(SSOBaseUrl[..<index])
                                if (subStr == "//") {
                                    cSSOBaseUrl = String(SSOBaseUrl.dropFirst(2))
                                } else {
                                    cSSOBaseUrl = SSOBaseUrl
                                }
                                
                                for cookie in cookies {
                                    if(cookie.name.uppercased().contains("ASPXAUTH") || cookie.name.lowercased().contains("expion.co") || cookie.name.lowercased().contains("ls.access")){
                                        var cookieProperties = [HTTPCookiePropertyKey:Any]()
                                        cookieProperties[HTTPCookiePropertyKey.name] = cookie.name
                                        cookieProperties[HTTPCookiePropertyKey.value] = cookie.value
                                        
                                        let cDomain: String = cookie.domain.lowercased().contains("oauth") ? cSSOBaseUrl : cookie.domain

                                        cookieProperties[HTTPCookiePropertyKey.domain] = cDomain
                                        cookieProperties[HTTPCookiePropertyKey.path] = cookie.path
                                        
                                        let newCookie = HTTPCookie(properties: cookieProperties)
                                        HTTPCookieStorage.shared.setCookie(newCookie!)
                                    }
                                }
                                
                                var cookieProperties = [HTTPCookiePropertyKey:Any]()
                                cookieProperties[HTTPCookiePropertyKey.name] = "ls.access"
                                
                                let originalString = String(format:"{\"access_token\":\"%@\"}", self.access_token)
                                let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                                //print(escapedString!)
                                
                                cookieProperties[HTTPCookiePropertyKey.value] = escapedString
                                cookieProperties[HTTPCookiePropertyKey.domain] = ".sysomos.com"
                                cookieProperties[HTTPCookiePropertyKey.path] = "/"
                                
                                let now = Date()
                                let tomorrow = Calendar.current.date(byAdding:
                                    .day, // updated this params to add hours
                                    value: 1,
                                    to: now)
                                
                                cookieProperties[HTTPCookiePropertyKey.expires] = tomorrow

                                let newCookie = HTTPCookie(properties: cookieProperties)
                                HTTPCookieStorage.shared.setCookie(newCookie!)
                                
                            }
                            
                            session.finishTasksAndInvalidate()
                            
                            self.loginComplete(company_id: company_id, SSOBaseUrl: SSOBaseUrl)
                        }
                    }
                } else {
                    //print("Load error getting notifications")
                }
                
            }
            
            task.resume()
        }
    }
    
    func loginComplete(company_id: Int, SSOBaseUrl: String) {
//        //print("loginComplete")
//        //print(company_id)
//        //print(SSOBaseUrl)
//        //print("---start oauth c---")
        let cookies = HTTPCookieStorage.shared.cookies!
        for cookie in cookies {
            let key: String = "ASPXAUTH"
//            //print("---oauth c---")
//            //print(cookie.name)
//            //print(cookie)
            if(cookie.name.contains(key)){
                let prefDefault = UserDefaults.standard
                prefDefault.set(cookie.value, forKey: key as String)
            }
        }
        
        
        var FEBaseUrl: String!
        //TODO: will need to modify this for production
        if(SSOBaseUrl.lowercased().contains("nate-")) {
            FEBaseUrl = String(format: "//%@-social.sysomos.com", self.m_oauthCode)
        } else {
            FEBaseUrl = SSOBaseUrl
        }
        
//        //print("---end oauth c---")
        delegate?.authenticationComplete!(aToken: access_token, companyId: company_id, SSOBaseUrl: SSOBaseUrl, FEBaseUrl: FEBaseUrl)
    }
}

extension AuthenticationLogin:URLSessionDelegate {
    /*
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    */
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//            //print("Using default handling")
            completionHandler(.performDefaultHandling, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        // We've got an error
        if let err = error {
            print("Error: \(err.localizedDescription)")
        } else {
            //print("Error. Giving up")
        }
    }
}

extension AuthenticationLogin:URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        // The task became a stream task - start the task
        //print("didBecome streamTask")
        streamTask.resume()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        // The task became a download task - start the task
        //print("didBecome downloadTask")
        downloadTask.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        // The original request was redirected to somewhere else.
        // We create a new dataTask with the given redirection request and we start it.
        let redirectUrl = request.url?.absoluteString
        print("")
        print("*--------------------*")
        print("willPerformHTTPRedirection")
        print("**")
        print("from->" + (response.url?.absoluteString)!)
        print("to->" + redirectUrl!)
        print("*--------------------*")
        print("")
        if(redirectUrl?.contains("http://"))! {
            
        }
        if let task = self.session?.dataTask(with: request) {
            task.resume()
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // We've got an error
        print("")
        print("X--E-Start--X")
        if let err = error {
            print("Error: \(err.localizedDescription)")
//            if error.code == NSURLErrorCancelled {
//                // canceled
//            } else {
//                // some other error
//            }
            if(err.localizedDescription.contains("offline") || err.localizedDescription.contains("certificate") || err.localizedDescription.contains("timed out")) {
                authenticationFailed(errorMessage: err.localizedDescription)
                self.session?.invalidateAndCancel()
            }
        } else {
//            //print("didCompleteWithError no error")
        }
        //            PlaygroundPage.current.finishExecution()
        print("X--E-End--X")
        print("")
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        // We've got the response headers from the server.
        //print("didReceive response")
        self.response = response
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    

    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        //print("urlSessionDidFinishEvents")
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        // We've got the response body
        //print("didReceive data")
        var responseTxt: String = ""
        if let responseText = String(data: data, encoding: .utf8) {
//            print("--1--")
//            print(self.response ?? "")
//            print("--2--")
//            print("\nServer's response text")
//            print(responseText)
//            print("--3--")
            let httpResponse = self.response as! HTTPURLResponse
            let statusCode = httpResponse.statusCode
            if(statusCode != 200) {
                responseTxt = String(describing: self.response)
                responseTxt.append("\n")
                responseTxt.append(responseText)
            }
        }
        
        var returnJSON: NSDictionary!
        
        do {
            //                returnJSON = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            returnJSON = try JSONSerialization.jsonObject(with: data as Data, options:[]) as? NSDictionary
            
            
            //                if let firstCategory = json["categories"]??[0]["name"] as? String {
            //                    //print("The first category in the JSON list is \"\(firstCategory)\"")
            //                }
        } catch let error as NSError {
            print("Error parsing JSON: \(error.localizedDescription)")
        }
        
        if returnJSON != nil {
            //print("returnJSON != nil=>")
            //print(returnJSON)
            
            //Notification stuff
//            if let unreadCount = returnJSON["unread_count"] as? Int {
//
//                print("returnJSON[\"unreadCount\"] found")
//                print(unreadCount)
//                self.delegate?.notificationsListRetreived!(notifications: returnJSON)
//            }
        } else {
//            //print("no returnJSON")
//            //print("url=>" + (self.response?.url?.absoluteString)!)
            let connectionSuccess: Bool = checkResponseUrl(responseUrl: (self.response?.url?.absoluteString)!, responseTxt: responseTxt)
            if (!connectionSuccess) {
                self.session?.invalidateAndCancel()
            }
        }
        
        self.session?.finishTasksAndInvalidate()
        //            PlaygroundPage.current.finishExecution()
    }
}

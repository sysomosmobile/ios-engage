//
//  ExpionVersion.swift
//  Workspaces
//
//  Created by Mike Bagwell on 7/16/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import UIKit
import Foundation

@objcMembers class ExpionVersion: NSObject {
    var vc:UIViewController!
    
    func makeVersionRequest(){
        //Make request for version info
        
        showForceUpdateAlert()
    }
    
    func checkResponseUrl(_ responseUrl: String){
        //Make request for version info
        //let urlStr = String(contentsOfURL: responseUrl)
        
        //println("checkResponseUrl =>" + responseUrl)
        if(responseUrl.lowercased().range(of: "mobileversioninvalid") != nil){
            //print("checkResponseUrl fail")
            showForceUpdateAlert()
        } else {
            //println("checkResponseUrl clear")
        }
    }
    
    func showForceUpdateAlert() {
        //print("showForceUpdateAlert")
        let alert = UIAlertController(title:"Update Required",
            message:"An update is required in order to use this application.",
            preferredStyle: UIAlertControllerStyle.alert)
        
        
        let updateAction = UIAlertAction(title:"Update",
                style:UIAlertActionStyle.default) { (UIAlertAction) -> Void in
                self.sendToAppStoreUpdate()
        }
        
        alert.addAction(updateAction)
        DispatchQueue.main.async {
            self.vc.present(alert, animated: true, completion: nil)
        }
    }
    
    func showVersionUpdateAlert() {
            let alert = UIAlertController(title:"Update Available",
                    message:"An update is available for this application.",
                    preferredStyle: UIAlertControllerStyle.alert)
            
            
            let updateAction = UIAlertAction(title:"Update",
                    style:UIAlertActionStyle.default) { (UIAlertAction) -> Void in
                    
            }
            
//            let cancelAction = UIAlertAction(title:"Cancel",
//            style:UIAlertActionStyle.default) { (UIAlertAction) -> Void in
//                self.sendToAppStoreUpdate()
//            }
            
            alert.addAction(updateAction)
            
            vc.present(alert, animated: true, completion: nil)
    }
    
    func sendToAppStoreUpdate(){
        //print("**sendToAppStoreUpdate**")
        let url  = URL(string: "https://geo.itunes.apple.com/us/app/sysomos-expion/id528135354?mt=8")
        
        if UIApplication.shared.canOpenURL(url!) {
            //print("**canOpenURL**")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)    //openURL(url!)
        } else {
            //print("!!canOpenURL**")
        }
    }


}

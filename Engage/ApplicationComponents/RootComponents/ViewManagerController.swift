//
//  ViewController.swift
//  Engage
//
//  Created by Mike Bagwell on 8/1/18.
//  Copyright © 2018 Expion. All rights reserved.
//

import UIKit

class ViewManagerController: UIViewController, LoginViewControllerDelegate {
    let shareData = ShareData.sharedInstance
    
    var loginViewController: LoginViewController?
    
    let serialQueue = DispatchQueue(label:"MainViewQueue")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setColorPallette()
        setUserAgent()
        showLogin(true)
    }

    func setColorPallette(){
        let colorStr = shareData.mwPrimaryColor
        shareData.primaryColor = ColorUtils().colorWithHexString(colorStr)
    }
    
    func setUserAgent() {
        let userAgentStr = AppSettings.userAgent
        print("- setUserAgent() -")
        print(userAgentStr)
        print("")
        
        shareData.userAgent = userAgentStr
        UserDefaults.standard.register(defaults: ["User-Agent": userAgentStr])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showLogin(_ autoLogin: Bool) {
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
        
        if (UserDefaults.standard.object(forKey: "accessToken") as? String) != nil {
            //            UserDefaults.standard.setValue("", forKey: "accessToken")
            UserDefaults.standard.removeObject(forKey: "accessToken")
        }
        if (UserDefaults.standard.object(forKey: "ASPXAUTH") as? String) != nil {
            //            UserDefaults.standard.setValue("", forKey: "ASPXAUTH")
            UserDefaults.standard.removeObject(forKey: "ASPXAUTH")
        }
        
        UserDefaults.standard.setValue("", forKey: "urlAPI")
        UserDefaults.standard.setValue("", forKey: "urlFE")
        UserDefaults.standard.setValue("", forKey: "companyId")
        UserDefaults.standard.setValue("", forKey: "userName")
        UserDefaults.standard.setValue(false, forKey: "debugMode") //debugMode.isOn
        UserDefaults.standard.synchronize()
        
        if(self.loginViewController != nil) {
            self.loginViewController = nil
        }
        
        self.loginViewController = UIStoryboard.loginViewController()
        self.loginViewController?.delegate = self
        self.loginViewController?.autoLogin = autoLogin
        
        DispatchQueue.main.async {
            let vc = self.view?.window?.rootViewController
            vc?.present(self.loginViewController!, animated: true, completion: nil)
        }
    }
    
    func loginComplete(){
        serialQueue.sync {
            self.setUserInfo()
        }
        serialQueue.sync {
            self.setupMainView()
        }
    }
    
    func setUserInfo() {
        self.loginViewController = nil
        
        let getAPIUrl:String = UserDefaults.standard.object(forKey: "urlAPI") as! String
        let getFEUrl:String = UserDefaults.standard.object(forKey: "urlFE") as! String
        let getCompany_id:String = UserDefaults.standard.object(forKey: "companyId") as! String
        let getUserName:String = UserDefaults.standard.object(forKey: "userName") as! String
        let getDebugMode:NSNumber = UserDefaults.standard.object(forKey: "debugMode") as! NSNumber
        
        
        var shouldResetCenterView = true
        
        if(shareData.urlAPI != nil && shareData.urlAPI == getAPIUrl && shareData.company_id != nil && shareData.company_id == getCompany_id && shareData.userName != nil && shareData.userName == getUserName){
            shouldResetCenterView = false
        }
        if(shouldResetCenterView == true){
            //            clearContainerView()
        }
        
        self.shareData.urlAPI = getAPIUrl
        self.shareData.urlFE = getFEUrl
        self.shareData.company_id = getCompany_id
        
        self.shareData.userName = getUserName
        self.shareData.debugMode = getDebugMode
        
        //print("END--loginComplete cookies")
    }
    
    func setupMainView() {
//
//        if(self.rightViewController != nil) {
//            self.rightViewController?.updateUserNameLabel()
//        }
//
//        if(centerViewController != nil){
//            refreshMLJSON()
//            centerViewController.setupCenterView()
//            DispatchQueue.main.async {
//                UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
//                    self.centerViewController.view.alpha = 1.0
//                })
//            }
//        } else {
//            initCenterView()
//        }
    }
}





private extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func loginViewController() -> LoginViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
    }
    
    class func bottomNavBarController() -> BottomNavBarViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "BottomNavBarViewController") as? BottomNavBarViewController
    }
}




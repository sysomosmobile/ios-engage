//
//  File.swift
//  Engage
//
//  Created by Mike Bagwell on 8/3/18.
//  Copyright © 2018 Expion. All rights reserved.
//

import UIKit

struct AppSettings {
    static var userAgent: String {
        let deviceStr = (AppSettings.isPad() == true ? "iPad" : "iPhone")
        let versionNumber = (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String).replacingOccurrences(of: ".", with: "")
        //        let versionNumber = "620100" //"650100"
        let originalUserAgent = UIWebView().stringByEvaluatingJavaScript(from: "navigator.userAgent")!
        let userAgentStr = "Expion-" + deviceStr + "-" + versionNumber + "-" + originalUserAgent
        return userAgentStr
    }
    
    static var TheCurrentDevice: UIDevice {
        struct Singleton {
            static let device = UIDevice.current
        }
        return Singleton.device
    }
    
    static var IS_IPAD: Bool {
        return isPad()
    }
    
    static func isPhone() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .phone
    }
    
    static func isPad() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .pad
    }
    
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
}

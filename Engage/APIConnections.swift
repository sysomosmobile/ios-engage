//
//  APIConnections.swift
//  Engage
//
//  Created by Mike Bagwell on 8/3/18.
//  Copyright © 2018 Expion. All rights reserved.
//

import Foundation

@objc(APIDataBaseClass) class APIDataBaseClass: NSObject {
    var url: String!
    var completion: ([Any]?)!
    
    override required init(){
        
    }
    
    required init(url: String, completion: ([Any]?)){
        self.url = url
        self.completion = completion
    }
}

@objc
protocol APIDataBaseClassDelegate {
    
}

@objc(MLNavAPI) class MLNavAPI: APIDataBaseClass {
    let aUrl: String = "/api/ml/navigation/12?display_mode="
    let aCompletion: ([Any]?)!
    
    var delegate: APIDataBaseClassDelegate?
    
    required init(){
        super.init()
    }
    
    required init(url: String, completion: ([Any]?)){
        super.init(url: aUrl, completion: aCompletion)
    }
    
    func parseMLJSON(_ responseData: NSMutableData, completion: (_ parsedMLData: Array<MLNavItem>) -> Void){
        var returnJSON: NSDictionary!
        do {
            returnJSON = try JSONSerialization.jsonObject(with: responseData as Data, options: []) as? NSDictionary
        } catch let error as NSError {
            print(error)
        }
        
        var returnItems: Array<MLNavItem> = []
        if returnJSON != nil {
            if let jsonArray = returnJSON["data"] as? Array<[String: Any]> {
                for dict in jsonArray{
                    let mlNavItem = MLNavItem(dict)
                    if(mlNavItem.display_name.lowercased().contains("workspaces") //){
                        //                        || mlNavItem.display_name.lowercased().contains("search")
                        || mlNavItem.display_name.lowercased().contains("publish")){
                        returnItems.append(mlNavItem)
                    }
                }
            } else {
                delegate?.mlJsonDataFailed("No MLJSON Data object")
            }
        } else {
            delegate?.mlJsonDataFailed("Failed to Parse MLJSON Data")
        }
        
        completion(returnItems)
    }
}

@objc(NavBarAPI) class NavBarAPI: APIDataBaseClass {
    let aUrl: String = "/api/ml/navigation/child/-546"
    let aCompletion: ([Any]?)!
    
    required init(){
        super.init()
    }
    
    required init(url: String, completion: ([Any]?)){
        super.init(url: aUrl, completion: aCompletion)
    }
}
    

class APIController {
    static let mlNavUrl: String = "/api/ml/navigation/12?display_mode="
    static let navBarUrl: String = "/api/ml/navigation/child/-546"
    
    
    func loadGet(){
        self.loadGetRequest(url, withCompletion: completion)
    }
    
    func loadGetRequest(_ urlString: String, withCompletion completion: @escaping ([Any]?) -> Void) {
        let session = URLSession.shared //URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: OperationQueue.main)
        let url = URL(string: urlString)!
        let task = session.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let data = data else {
                completion(nil)
                return
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
                completion(nil)
                return
            }
            let result: [Any]
            switch urlString {
            case APIController.mlNavUrl:
                result = [] // Transform JSON into Question values
            case APIController.navBarUrl:
                result = [] // Transform JSON into Question values
            default:
                result = []
            }
            completion(result)
        })
        task.resume()
    }
}

//
//  ShareData.swift
//  Engage
//
//  Created by Mike Bagwell on 8/3/18.
//  Copyright © 2018 Expion. All rights reserved.
//

import UIKit

class ShareData {
    
    //    private static var __once: () = {
    //            Static.instance = ShareData()
    //        }()
    //    class var sharedInstance: ShareData {
    //        struct Static {
    //            static var instance: ShareData?
    //            static var token: Int = 0
    //        }
    //
    //        _ = ShareData.__once
    //
    //        return Static.instance!
    //    }
    
    static let sharedInstance : ShareData = {
        let instance = ShareData()
        return instance
    }()
    
    var company_id: String!
    var urlAPI: String!
    var urlFE: String!
    var debugMode: NSNumber!
    var notificationsEnabled: NSNumber!
    var isAdmin: NSNumber!
    var userName: String!
    
    
    var userId: NSNumber!
    //    var apns_token: String!
    var locationLabel: String = "Location"
    var transitionSpeed: TimeInterval = 0.42 //1.0
    
    var userAgent: String!
    //    var notifEnabled: Bool = false
    
    let mwPrimaryColor = "#28bbbb"
    let sysPrimaryColor = "#2797D4"
    var primaryColor: UIColor!
}
